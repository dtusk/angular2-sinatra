/// <reference path="typings/angular2/angular2.d.ts" />
/// <reference path="typings/jquery/jquery.d.ts" />
if (typeof __decorate !== "function") __decorate = function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
if (typeof __metadata !== "function") __metadata = function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var angular2_1 = require('angular2/angular2');
var AppComponent = (function () {
    function AppComponent() {
        var _this = this;
        $.get('/api/fights', function (data) { return _this.fights = data; });
    }
    AppComponent.prototype.addFight = function (fighter1, fighter2, won) {
        var _this = this;
        var fight = { fighter1: fighter1, fighter2: fighter2, won: won };
        $.post('/api/fights', fight, function (data) { return _this.fights.push(data); });
    };
    // http://www.reddit.com/r/angularjs/comments/2zn9qs/angular_2_does_not_support_twoway_databindings/
    AppComponent.prototype.editFight = function (id, fighter1, fighter2, won) {
        $.ajax({
            type: 'PUT',
            url: '/api/fights/' + id,
            dataType: 'JSON',
            data: { fighter1: fighter1, fighter2: fighter2, won: won },
            success: function () {
                console.log('sukces');
            }
        });
    };
    AppComponent.prototype.deleteFight = function (id) {
        $.ajax({
            type: "DELETE",
            url: "/api/fights/" + id,
            success: function () {
                $("#" + id).remove();
            }
        });
    };
    AppComponent = __decorate([
        angular2_1.Component({
            selector: 'app'
        }),
        angular2_1.View({
            templateUrl: 'views/fights.html',
            directives: [angular2_1.For, angular2_1.If]
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
})();
angular2_1.bootstrap(AppComponent);
