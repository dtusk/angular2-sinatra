/// <reference path="typings/angular2/angular2.d.ts" />
/// <reference path="typings/jquery/jquery.d.ts" />

import {Component, View, bootstrap, For, If} from 'angular2/angular2';

@Component({
  selector: 'app'
})

@View({
  templateUrl: 'views/fights.html',
  directives: [For, If]
})

// Component controller
class AppComponent {

  fights: List<Object>;

  constructor() {
    $.get('/api/fights', (data) => this.fights = data);
  }

  addFight(fighter1: string, fighter2: string, won: number) {
    var fight =  {fighter1: fighter1, fighter2: fighter2, won: won};
    $.post('/api/fights', fight, (data) => this.fights.push(data));
  }

// http://www.reddit.com/r/angularjs/comments/2zn9qs/angular_2_does_not_support_twoway_databindings/
  editFight(id: number, fighter1: string, fighter2: string, won: number) {
    $.ajax({
      type: 'PUT',
      url: '/api/fights/' + id,
      dataType: 'JSON',
      data: { fighter1: fighter1, fighter2: fighter2, won: won },
      success: function() {
        console.log('sukces');
      }
    });
  }

  deleteFight(id: number) {
    $.ajax({
      type: "DELETE",
      url: "/api/fights/" + id,
      success: function() {
        $("#" + id).remove();
      }
    });
  }
}

bootstrap(AppComponent);
